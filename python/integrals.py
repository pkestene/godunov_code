# import classes
#
from numpy import array

def read_integrals(filename, column):
    '''
	read_integrals: reads a given column from an integral file.
    '''
    # Open the given file and check if it is text file.
    #
    f = open(filename, 'r')

    # Read fist line is store it in h, since it will be used to obtain the
    # column headers.
    #
    l = f.readline()
    h = l

    # Read first line which is not comment in order to determine the number of
    # columns and store the number of columns in nc.  Calculate the column width
    # and store it in wc.
    #
    while l.startswith('#'):
	l = f.readline()
    nc = len(l.rsplit())
    ic = l.index(l.rsplit()[0])
    wc = (len(l) - ic) / (nc - 1)

    # Split header line into a list.
    #
    lh = [h[1:ic+1].strip()]
    for i in range(0, nc - 1):
      ib = i * wc + ic + 1
      ie = (i + 1) * wc + ic + 1
      lh.append(h[ib:ie].strip())

    ic = lh.index(column)

    # Read given column.
    #
    if (ic > -1):
      lc = [float(l.split()[ic])]
      for l in f:
        if not l.startswith('#'):
          lc.append(float(l.split()[ic]))

    # Close the file.
    #
    f.close()

    # Return values.
    #
    return(array(lc))
