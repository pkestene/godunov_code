;===============================================================================
;
;  function READHDF5_VARIABLE reads variables from HDF5 data cubes produced
;  by the Godunov code.
;
;-------------------------------------------------------------------------------
;
;  arguments:
;
;    file        - the file name of the HDF5 data snapshot
;    variable    - the variable name
;    shrink      - the restriction factor, meaning that the data should be
;                  rescaled down (only integer numbers larger than 1)
;    list        - keyword /LIST to list coordinates stored in the file
;    single      - keyword /SINGLE to limit reading from one file only
;                  without merging the whole domain
;
function readhdf5_variable, file, variable                                     $
                          , list = list, single = single, shrink = shrink  $
                          , quiet = quiet

; initialize variables
;
  array = -1
  ncpus = 1

; check it the specified file exists
;
  if (not file_test(file, /regular, /read)) then begin

    print, file + ' does not exist!'

    return, array

  endif

; check it the file is really written in the HDF5 format
;
  if (not h5f_is_hdf5(file)) then begin

    print, file + ' is not a HDF5 file!'

    return, array

  endif

; check if the arguments are correct
;
  if (not keyword_set(shrink)) then shrink = 1

; open the file
;
  fid = h5f_open(file)

; check if there were problems with opening the SD interface
;
  if (fid le 0) then begin

    print, 'Could not open ' + file

; close the file
;
    h5f_close, fid

; return -1
;
    return, -1

  endif

; everything goes fine so far, so proceed
;
; if keyword /LIST is defined list all variables
;
  if (keyword_set(list)) then begin

; open the group of variables
;
    gid = h5g_open(fid, 'variables')

; get the number of datasets
;
    ndatasets = h5g_get_num_objs(gid)

; print all dataset names
;
    print, 'Number of variables:', ndatasets
    names = strarr(ndatasets)
    for n = 0, ndatasets - 1 do begin
      nm = h5g_get_obj_name_by_idx(gid, n)
      names[n] = nm
    endfor
    print, 'Variable names     :', names

; close the group
;
    h5g_close, gid

; close the file
;
    h5f_close, fid

; return 0
;
    return, 0

  endif

; open the group of attributes
;
  gid = h5g_open(fid, 'attributes')

; read attributes
;
  nprocs = 1

; get the number of attributes
;
  nattributes = h5a_get_num_attrs(gid)

; read attributes
;
  for n = 0, nattributes - 1 do begin
    aid = h5a_open_idx(gid, n)
    name = h5a_get_name(aid)
    data = h5a_read(aid)
    case name of
      'nprocs' : nprocs = data[0]
    else :
    endcase
    h5a_close, aid
  endfor

; close the group
;
  h5g_close, gid

; proceed if ncpus == 1 or kyeword /SINGLE is set
;
  if (nprocs eq 1 or keyword_set(single)) then begin ; data in a single file

; open the group of variables
;
    gid = h5g_open(fid, 'variables')

; get the number of datasets
;
    ndatasets = h5g_get_num_objs(gid)

; find variables
;
    for n = 0, ndatasets - 1 do begin
      name = h5g_get_obj_name_by_idx(gid, n)
      if (name eq variable) then begin
        did   = h5d_open(gid, name)
        array = h5d_read(did)
        h5d_close, did
      endif
    endfor

; variable not found
;
    if (n_elements(array) eq 1) then begin
      if (not keyword_set(quiet)) then begin
        print, 'Could not find variable ', variable, ' in the file.'
        print, 'Please add keyword /INFO to see all stored variables.'
      endif
      h5g_close, gid
      h5f_close, fid
      return, -1
    endif

; close the group
;
    h5g_close, gid

; close the file
;
    h5f_close, fid

  endif else begin

; open the group of variables
;
    gid = h5g_open(fid, 'variables')

; get the number of datasets
;
    ndatasets = h5g_get_num_objs(gid)

; find the index of requested dataset
;
    dset_index = -1
    for n = 0, ndatasets - 1 do begin
      name = h5g_get_obj_name_by_idx(gid, n)
      if (name eq variable) then begin
        dset_index = n
      endif
    endfor

; variable not found
;
    if (dset_index eq -1) then begin
      if (not keyword_set(quiet)) then begin
        print, 'Could not find variable ', variable, ' in the file.'
        print, 'Please add keyword /INFO to see all stored variables.'
      endif
      h5g_close, gid
      h5f_close, fid
      return, -1
    endif

; close the group
;
    h5g_close, gid

; obtain information about the domain dimensions and division
;
    pdims = [1, 1, 1]
    dims  = [1, 1, 1]
    ng    = 0
    nproc = 0

; open the group of attributes
;
    gid = h5g_open(fid, 'attributes')

; read attributes
;
    for n = 0, nattributes - 1 do begin
      aid = h5a_open_idx(gid, n)
      name = h5a_get_name(aid)
      data = h5a_read(aid)
      case name of
        'pdims'  : pdims  = data
        'dims'   : dims   = data
        'ng'     : ng     = data[0]
        'nproc'  : nproc  = data[0]
      else :
      endcase
      h5a_close, aid
    endfor

; close the group
;
    h5g_close, gid

; calculate the size of full array
;
    ng = replicate(ng, n_elements(dims))
    ii = where(dims eq 1, c)
    if (c ge 1) then ng[ii] = 0
    sz = (dims - 2 * ng)
    rz = sz / shrink

; create the variable array
;
    tmp = fltarr(pdims * rz)

; close the file
;
    h5f_close, fid

; iterate over all files and read data
;
    pos = strpos(file, string(nproc, format = '(i5.5,".h5")'))

    for p = 0, nprocs-1 do begin

; prepare the filename
;
    strput, file, string(p, format = '(i5.5)'), pos

; open the file
;
      fid = h5f_open(file)

; open the group of attributes
;
      gid = h5g_open(fid, 'attributes')

      for n = 0, nattributes - 1 do begin
        aid = h5a_open_idx(gid, n)
        name = h5a_get_name(aid)
        data = h5a_read(aid)
        case name of
          'pcoords': pcoords  = data
        else :
        endcase
        h5a_close, aid
      endfor

; close the group
;
      h5g_close, gid

; open the group of variables
;
      gid = h5g_open(fid, 'variables')

; find variables
;
      for n = 0, ndatasets - 1 do begin
        name = h5g_get_obj_name_by_idx(gid, n)
        if (name eq variable) then begin
          did   = h5d_open(gid, name)
          array = h5d_read(did)
          h5d_close, did
        endif
      endfor

; close the group
;
      h5g_close, gid

; close the file
;
      h5f_close, fid

; insert sub-cube into global array
;
      ib = ng[0]
      ie = ib + sz[0] - 1
      jb = ng[1]
      je = jb + sz[1] - 1
      kb = ng[2]
      ke = kb + sz[2] - 1

      array = temporary(array[ib:ie,jb:je,kb:ke])

      ib = pcoords[0] * rz[0]
      ie = ib + rz[0] - 1
      jb = pcoords[1] * rz[1]
      je = jb + rz[1] - 1
      kb = pcoords[2] * rz[2]
      ke = kb + rz[2] - 1

      tmp[ib:ie,jb:je,kb:ke] = rebin(array, rz, /sample)

    endfor

    array = tmp

  endelse

; return the variable array
;
  return, array

end
