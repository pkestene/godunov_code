;===============================================================================
;
;  function READHDF5_ATTRIBUTE reads attributes from HDF5 data cubes produced
;  by the Godunov code.
;
;-------------------------------------------------------------------------------
;
;  arguments:
;
;    file        - the file name of the HDF5 data snapshot
;    attribute   - the attribute name
;
function readhdf5_attribute, file, attribute, list = list

; initialize variables
;
  array = -1

; check it the file exists
;
  if (not file_test(file, /regular, /read)) then begin

    print, file + ' does not exist!'

    return, array

  endif

; check it the file is really written in HDF format
;
  if (not h5f_is_hdf5(file)) then begin

    print, file + ' is not a HDF5 file!'

    return, array

  endif

; open a file
;
  fid = h5f_open(file)

; check if there were problems with opening the SD interface
;
  if (fid le 0) then begin

    print, 'Could not open ' + file

; close the file
;
    h5f_close, fid

; return -1
;
    return, -1

  endif

; everything goes fine so far, so proceed with the file
;
; if keyword /LIST is defined list all variables only
;
  if (keyword_set(list)) then begin

; open group 'attributes'
;
    gid = h5g_open(fid, 'attributes')

; get number of attributes
;
    nattributes = h5a_get_num_attrs(gid)

; print all attribute names
;
    print, 'Number of attributes:', nattributes
    attrs = strarr(nattributes)
    for n = 0, nattributes - 1 do begin
      aid = h5a_open_idx(gid, n)
      nm = h5a_get_name(aid)
      h5a_close, aid
      attrs[n] = nm
    endfor
    print, 'Attribute names     :', attrs

; close group 'attributes'
;
    h5g_close, gid

; close the file
;
    h5f_close, fid

; return 0
;
    return, 0

  endif

; open group 'attributes'
;
  gid = h5g_open(fid, 'attributes')

; get number of attributes
;
  nattributes = h5a_get_num_attrs(gid)

; read attributes
;
  for n = 0, nattributes - 1 do begin

    aid = h5a_open_idx(gid, n)
    name = h5a_get_name(aid)
    data = h5a_read(aid)
    h5a_close, aid

; check if we found the correct attribute
;
    if (name eq attribute) then array = reform(data)

  endfor

; close group 'attributes'
;
  h5g_close, gid

; close the file
;
  h5f_close, fid

; return the attribute
;
  return, array

end
