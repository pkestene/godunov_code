;===============================================================================
;
;  function READBIN_ATTRIBUTE reads attributes from binary data cubes produced
;  by the Godunov code.
;
;-------------------------------------------------------------------------------
;
;  arguments:
;
;    file        - the file name of the HDF5 data snapshot
;    attribute   - the attribute name
;
function readbin_attribute, file, attribute, list = list

; initialize variables
;
  array   = -1
  nchunks =  1l
  nchunk  =  0l
  pdims   = [ 1l, 1l, 1l ]
  pcoords = [ 0l, 0l, 0l ]

; check it file exists
;
  if (not file_test(file, /regular, /read)) then begin

    print, file + ' does not exist!'

; return -1
;
    return, array

  endif

; open the snapshot file
;
  openr, fid, file, /get_lun, /f77_unformatted

; check if there were problems with opening the SD interface
;
  if (fid le 0) then begin

    print, 'Could not open ' + file

; close the file
;
    close, fid

; release the file handler
;
    free_lun, fid

; return -1
;
    return, array

  endif

; read first 8 characters and check if the file is a Godunov snapshot
;
  block = '            '
  readu, fid, block

; check it the file is really written in HDF format
;
  if (strtrim(block) ne 'godunov:') then begin

    print, file + ' is not a Godunov snapshot file!'

; close the file
;
    close, fid

; release the file handler
;
    free_lun, fid

; return -1
;
    return, array

  endif

; read the domain division attributes
;
  readu, fid, nchunks, nchunk
  readu, fid, pdims
  readu, fid, pcoords

; read next block
;
  next:

  readu, fid, block

  case(strtrim(block)) of
    'evolution:': begin
                    step = 0l
                    time = 0.0d+00
                    dt   = 0.0d+00
                    dtn  = 0.0d+00

                    readu, fid, step
                    readu, fid, time, dt, dtn
                  end
    'dimensions:': begin
                    in = 0l
                    jn = 0l
                    kn = 0l
                    nn = 0l

                    readu, fid, in, jn, kn
                    readu, fid, nn
                  end
    else: goto, finish
  endcase

; go to the next block
;
  goto, next

  finish:

; close the file
;
  close, fid

; release the file handler
;
  free_lun, fid

; choose the right coordinate
;
  case strtrim(attribute) of
    'step': array = step
    'time': array = time
    'dt'  : array = dt
    'dtn' : array = dtn
    else:
  endcase

; return the variable array
;
  return, array

end
