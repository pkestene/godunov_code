;===============================================================================
;
;  function READBIN_COORDINATE reads coordinates from binary data cubes produced
;  by the Godunov code.
;
;-------------------------------------------------------------------------------
;
;  arguments:
;
;    file        - the file name of the HDF5 data snapshot
;    coordinate  - the coordinate name (xc, yc, zc, xi, yi, or zi)
;    shrink      - the restriction factor, meaning that the data should be
;                  rescaled down (only integer numbers larger than 1)
;    list        - keyword /LIST to list coordinates stored in the file
;    single      - keyword /SINGLE to limit reading from one file only
;                  without merging the whole domain
;
function readbin_coordinate, file, coordinate                                  $
                           , shrink = shrink, list = list, single = single

; initialize variables
;
  array   = -1
  nchunks =  1l
  nchunk  =  0l
  pdims   = [ 1l, 1l, 1l ]
  pcoords = [ 0l, 0l, 0l ]

; check it the file specified exists
;
  if (not file_test(file, /regular, /read)) then begin

    print, file + ' does not exist!'

; return -1
;
    return, array

  endif

; open the snapshot file
;
  openr, fid, file, /get_lun, /f77_unformatted

; check if there were problems with opening the file
;
  if (fid le 0) then begin

    print, 'Could not open ' + file

; close the file
;
    close, fid

; release the file handler
;
    free_lun, fid

; return -1
;
    return, array

  endif

; check if arguments are correct
;
  if (not keyword_set(shrink)) then shrink = 1

; read first 8 characters and check if the file is a Godunov snapshot
;
  block = '            '
  readu, fid, block

; check it the file is really written in HDF format
;
  if (strtrim(block) ne 'godunov:') then begin

    print, file + ' is not a Godunov snapshot file!'

; close the file
;
    close, fid

; release the file handler
;
    free_lun, fid

; return -1
;
    return, array

  endif

; read the domain division attributes
;
  readu, fid, nchunks, nchunk
  readu, fid, pdims
  readu, fid, pcoords

; read next block
;
  next:

  readu, fid, block

  case(strtrim(block)) of
    'evolution:': begin
                    step = 0l
                    time = 0.0d+00
                    dt   = 0.0d+00
                    dtn  = 0.0d+00

                    readu, fid, step
                    readu, fid, time, dt, dtn
                  end
    'dimensions:': begin
                    in = 0l
                    jn = 0l
                    kn = 0l
                    nn = 0l

                    readu, fid, in, jn, kn
                    readu, fid, nn
                  end
    else: goto, finish
  endcase

; go to the next block
;
  goto, next

  finish:

; close the file
;
  close, fid

; release the file handler
;
  free_lun, fid

; we have all informations now to reconstruct the variable array from all
; chunks, so prepare the variable array dimension
;
  pm = [in, jn, kn]
  dm = pdims * pm
  pm = pm / shrink
  rm = dm / shrink
  if (dm[2] eq 1) then pm[2] = 1
  if (dm[2] eq 1) then rm[2] = 1

; allocate the array
;
  xc = fltarr(rm[0])
  yc = fltarr(rm[1])
  zc = fltarr(rm[2])

; find the position which varies between different file names
;
  pos = strpos(file, string(nchunk, format = '(i5.5,".bin")'))

; iterate over all chunks
;
  for p = 0, nchunks - 1 do begin

; prepare the chunk filename
;
    strput, file, string(p, format = '(i5.5)'), pos

; open the chunk file
;
    openr, cid, file, /get_lun, /f77_unformatted

; read next chunk
;
    back:
    readu, fid, block

    case(strtrim(block)) of

; read the domain division attributes
;
      'godunov:':      begin
                         nchks =  1l
                         nchk  =  0l
                         pdms  = [ 1l, 1l, 1l ]
                         pcrds = [ 0l, 0l, 0l ]

                         readu, cid, nchs, nchk
                         readu, cid, pdms
                         readu, cid, pcrds
                       end
      'evolution:':    begin
                         step = 0l
                         time = 0.0d+00
                         dt   = 0.0d+00
                         dtn  = 0.0d+00

                         readu, cid, step
                         readu, cid, time, dt, dtn
                       end
      'dimensions:':   begin
                        in = 0l
                        jn = 0l
                        kn = 0l
                        nn = 0l

                        readu, cid, in, jn, kn
                        readu, cid, nn
                      end
      'coordinates:': begin
                        nc = '  '
                        tc = fltarr(in)
                        readu, cid, nc
                        readu, cid, tc
                        ib = pcrds[0] * pm[0]
                        ie = ib + pm[0] - 1
                        xc[ib:ie] = 0.0
                        for i = 0, shrink - 1 do $
                          xc[ib:ie] = xc[ib:ie] + tc[i:*:shrink] / shrink
                        tc = fltarr(jn)
                        readu, cid, nc
                        readu, cid, tc
                        jb = pcrds[1] * pm[1]
                        je = jb + pm[1] - 1
                        yc[jb:je] = 0.0
                        for j = 0, shrink - 1 do $
                          yc[jb:je] = yc[jb:je] + tc[j:*:shrink] / shrink
                        tc = fltarr(kn)
                        readu, cid, nc
                        readu, cid, tc
                        kb = pcrds[2] * pm[2]
                        ke = kb + pm[2] - 1
                        zc[kb:ke] = 0.0
                        if (kn gt 1) then $
                          for k = 0, shrink - 1 do $
                            zc[kb:ke] = zc[kb:ke] + tc[k:*:shrink] / shrink
                        goto, done
                      end
      else: goto, done
    endcase

    goto, back

    done:

; close the chunk file
;
    close, cid

; release the file handler
;
    free_lun, cid

  endfor

; choose the right coordinate
;
  case strtrim(coordinate) of
    'xc': array = xc
    'yc': array = yc
    'zc': array = zc
    else:
  endcase

; return the variable array
;
  return, array

end
