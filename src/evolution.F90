!!******************************************************************************
!!
!!  This file is part of the GODUNOV source code, a program to perform
!!  Newtonian or relativistic magnetohydrodynamical simulations.
!!
!!  Copyright (C) 2007-2016 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! module: EVOLUTION
!!
!!  This module performs the time integration using different methods.
!!
!!******************************************************************************
!
module evolution

#ifdef PROFILE
! include external subroutines
!
  use timers, only : set_timer, start_timer, stop_timer
#endif /* PROFILE */

! module variables are not implicit by default
!
  implicit none

! pointer to the temporal integration subroutine
!
  procedure(evolve_euler), pointer, save :: evolve => null()

#ifdef PROFILE
! timer indices
!
  integer, save :: iad, its
#endif /* PROFILE */

! evolution parameters
!
  integer, save :: nstages = 2
  integer, save :: nbase   = 2
  real   , save :: cfl     = 0.5d+00

#ifdef GLM
! coefficient controlling the decay of scalar potential ѱ
!
  real   , save :: alpha   = 1.0d+00
  real   , save :: decay   = 1.0d+00
#endif /* GLM */

! by default everything is private
!
  private

! declare public subroutines
!
  public :: initialize_evolution, finalize_evolution, advance, new_time_step

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
  contains
!
!===============================================================================
!
! subroutine INITIALIZE_EVOLUTION:
! -------------------------------
!
!   Subroutine initializes the evolution module by setting its parameters
!   and pointers to the time integration methods.
!
!
!===============================================================================
!
  subroutine initialize_evolution(verbose)

! include external procedures and variables
!
    use mesh      , only : time
    use parameters, only : get_parameter_string, get_parameter_real            &
                         , get_parameter_integer

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    logical, intent(in) :: verbose

! local variables
!
    character(len=255) :: integration = "rk2"
    character(len= 26) :: iname       = "Time advance"
    character(len=255) :: name_int    = ""
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! set timer descriptions
!
    call set_timer('solution advance'    , iad)
    call set_timer('time step estimation', its)
#endif /* PROFILE */

! get the integration method, parameters, and the value of the CFL coefficient
!
    call get_parameter_string ("time_advance", integration)
    call get_parameter_integer("nstages"     , nstages    )
    call get_parameter_integer("nbase"       , nbase      )
    call get_parameter_real   ("cfl"         , cfl        )
#ifdef GLM
    call get_parameter_real   ("alpha"       , alpha      )
#endif /* GLM */

! check the correctness of the integration parameters
!
    if (cfl <= 0.0d+00) then
      if (verbose) then
        write(*,*) "Incorrect value of the CFL coefficient!"
        write(*,*) "Setting to the default value cfl = 0.5 ..."
        write(*,*)
      end if

      cfl = 0.5d+00
    end if

! select the integration method, check the correctness of the integration
! parameters and adjust the CFL coefficient if necessary
!
    select case(trim(integration))

    case ("euler", "EULER")

      name_int =  "1st order Euler method"
      evolve   => evolve_euler

    case ("rk2", "RK2")

      name_int =  "2nd order Runge-Kutta method"
      evolve   => evolve_rk2

    case ("rk2.n", "RK2.N")

      if (nstages < 2) then
        if (verbose) then
          write(*,*) "The number of Runge-Kutta stages is too low!"
          write(*,*) "Setting to the minimum value nstages = 2... "
          write(*,*)
        end if

        nstages = 2
      end if

      write(name_int,"(i4)") nstages
      name_int = "2nd order Runge-Kutta method (" // trim(adjustl(name_int))   &
                                                  // "-stage)"
      evolve   => evolve_rk2_n

      cfl = (nstages - 1) * cfl

    case ("rk3", "RK3")

      name_int =  "3rd order Runge-Kutta method"
      evolve   => evolve_rk3

    case ("rk3.4", "RK3.4")

      name_int =  "3rd order Runge-Kutta method (4-stage)"
      evolve   => evolve_rk3_4

      cfl = 2.0d+00 * cfl

    case ("rk3.n", "RK3.N")

      if (nbase < 2) then
        if (verbose) then
          write(*,*) "The base for the number of Runge-Kutta stages is too low!"
          write(*,*) "Setting to the minimum value nbase = 2 ..."
          write(*,*)
        end if

        nbase = 2
      end if

      write(name_int,"(i4)") nbase * nbase
      name_int = "3rd order Runge-Kutta method (" // trim(adjustl(name_int))   &
                                                  // "-stage)"
      evolve   => evolve_rk3_n

      cfl = (nbase   - 1) * nbase * cfl

    case ("rk4.10", "RK4.10")

      name_int =  "4th order Runge-Kutta method (10-stage)"
      evolve   => evolve_rk4_10

      cfl = 6.0d+00 * cfl

    case default

      if (verbose) then
        write (*,"(1x,a)") "The selected time advance method is not " //       &
                           "implemented: " // trim(integration)
        name_int =  "2nd order Runge-Kutta method"
        evolve   => evolve_rk2
      end if

    end select

#ifdef GLM
! calculate the decay factor for magnetic field divergence scalar source term
!
    decay = exp(- alpha * cfl)
#endif /* GLM */

! print information about the temporal integration method
!
    if (verbose) then
      write (*,"(1x,a26,':',1x,a)") adjustl(iname), trim(name_int)
    end if

!-------------------------------------------------------------------------------
!
  end subroutine initialize_evolution
!
!===============================================================================
!
! subroutine FINALIZE_EVOLUTION:
! -----------------------------
!
!   Subroutine finalizes the evolution module by releasing all memory used
!   by its module variables.
!
!
!===============================================================================
!
  subroutine finalize_evolution()

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
! release the procedure pointers
!
    nullify(evolve)

!-------------------------------------------------------------------------------
!
  end subroutine finalize_evolution
!
!===============================================================================
!
! subroutine ADVANCE:
! ------------------
!
!   Subroutine first updates the new time step and then calls the proper method
!   in order to advance the solution to the next time step.
!
!
!===============================================================================
!
  subroutine advance(dtnext)

! declare all variables as implicit
!
    implicit none

! input variables
!
    real, intent(in) :: dtnext
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the evolution
!
    call start_timer(iad)
#endif /* PROFILE */

! estimate the new time step
!
    call new_time_step(dtnext)

! perform one step of the time evolution
!
    call evolve()

#ifdef PROFILE
! stop accounting time for the evolution
!
    call stop_timer(iad)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine advance
!
!===============================================================================
!
! subroutine NEW_TIME_STEP:
! ------------------------
!
!   Subroutine estimates the new time step from the maximum speed in the system
!   and source term constraints.
!
!
!===============================================================================
!
  subroutine new_time_step(dtnext)

! obtain required subroutines from other modules
!
    use algebra  , only : quadratic
    use equations, only : maxspeed
#ifdef FORCING
    use forcing  , only : facc, dtfor
#endif /* FORCING */
    use mesh     , only : dt, dtn, dth, dta, dtd, dxmin
    use mesh     , only : ib, ie, jb, je, kb, ke
#ifdef MPI
    use mpitools , only : reduce_minimum_real, reduce_maximum_real
#endif /* MPI */
    use sources   , only : visc
#ifdef RESISTIVITY
    use sources   , only : ueta
#endif /* RESISTIVITY */
    use variables , only : idn
    use variables , only : u0
#if defined RESISTIVITY && defined ANOMALOUS
    use variables , only : qp
    use variables , only : iet
#endif /* RESISTIVITY & ANOMALOUS */
    use variables , only : amax, cmax, cmax2

! local variables are not implicit by default
!
    implicit none

! input variables
!
    real, intent(in) :: dtnext

! local variables
!
    integer         :: iret
    logical, save   :: first = .true.
    real   , save   :: dthyd
    real   , save   :: diff, dtdif, fcdif
    integer         :: i, j, k
    real            :: dnmin, dnmax
#ifdef RESISTIVITY
    real, save      :: etmax
#endif /* RESISTIVITY */

! arrays for quadratic solver
!
    integer            :: nr
    real, dimension(3) :: a
    real, dimension(2) :: x

! local parameters
!
    real, parameter    :: eps = epsilon(dthyd)
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the new time step estimation
!
    call start_timer(its)
#endif /* PROFILE */

! calculate the time step constraint from constant coefficients
!
    if (first) then

#ifdef RESISTIVITY
! set the maximum resistivity
!
      etmax = max(eps, ueta)
#endif /* RESISTIVITY */

! calculate the diffusion factor
!
      fcdif = dxmin * dxmin

! reset the first run flag
!
      first = .false.

    end if

! reset the maximum acceleration
!
   amax = 0.0d+00

#ifdef FORCING
! update the maximum acceleration by the acceleration resulting from the forcing
!
   amax = amax + facc

#ifdef MPI
! find the maximum acceleration in the system from all processors
!
    call reduce_maximum_real(amax, iret)
#endif /* MPI */
#endif /* FORCING */

! determine maximum speed in the system
!
    cmax  = maxspeed()

#ifdef MPI
! find maximum speed in the system from all processors
!
    call reduce_maximum_real(cmax, iret)
#endif /* MPI */

! prepare coefficients to find the CFL time step
!
    a(1)  = - dxmin
    a(2)  = cmax
    a(3)  = 0.5d+00 * amax

! solve the quadratic equation to find the CFL time step
!
    nr    = quadratic(a(:), x(:))

! estimate new time step from the maximum speed in the system
!
    if (nr > 0) then
      dthyd = x(nr)
    else
      dthyd = dxmin / max(eps, cmax)
    end if

! initial value of diffusion coefficient
!
    diff  = eps

! find minimum and maximum densities, necessary to determine the von Neumann
! number
!
    if (visc > 0.0d+00) then
      dnmin = huge(dnmin)
      dnmax = 0.0d+00
      do k = kb, ke
        do j = jb, je
          do i = ib, ie
            dnmin = min(dnmin, u0(idn,i,j,k))
            dnmax = max(dnmax, u0(idn,i,j,k))
          end do
        end do
      end do
#ifdef MPI
! reduce maximum density
!
      call reduce_minimum_real(dnmin, iret)
      call reduce_maximum_real(dnmax, iret)
#endif /* MPI */

! contribution from the viscosity coefficient
!
      diff  = diff + max(0.0d+00, visc / dnmin, visc * dnmax)

    end if ! visc > 0

#ifdef RESISTIVITY
#ifdef ANOMALOUS
! find maximum resistivity
!
    etmax = 0.0d+00
    do k = kb, ke
      do j = jb, je
        do i = ib, ie
          etmax = max(etmax, qp(iet,i,j,k))
        end do
      end do
    end do
#ifdef MPI
! reduce maximum resistivity
!
    call reduce_maximum_real(etmax, iret)
#endif /* MPI */
#endif /* ANOMALOUS */

! contribution from the resistivity coefficient
!
    diff  = diff + etmax
#endif /* RESISTIVITY */

! calculate the time step from diffusion
!
    dtdif = fcdif / diff

! substitute the new time step
!
    dth = dthyd
    dta = sqrt(2.0d+00 * dxmin / max(eps, amax))
    dtd = dtdif
    dtn = min(dthyd, dtdif)

! calculate the new time step
!
    dt = cfl * dtn

#if defined FORCING && defined ALVELIUS
! round time step to multiplicity of dtfor
!
    dt = max(dt - mod(dt, dtfor), dtfor)
#endif /* FORCING & ALVELIUS */

! round the time
!
    if (dtnext > 0.0d+00) dt = min(dt, dtnext)

! calculate the square of cmax
!
    cmax2 = (cmax + amax * dt)**2

#ifdef PROFILE
! stop accounting time for the new time step estimation
!
    call stop_timer(its)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine new_time_step
!
!===============================================================================
!
! subroutine UPDATE_VARIABLES:
! ---------------------------
!
!   Subroutine updates all variables, by converting conservative variables to
!   their primitive representation, applying shapes and updating the boundaries.
!
!   Arguments:
!
!     dtt - fractional time step needed to update time varying boundary
!           conditions;
!
!===============================================================================
!
  subroutine update_variables(dtt)

! include external procedures and variables
!
    use boundaries    , only : update_boundaries
#ifdef BFIELD
    use boundaries    , only : update_magnetic_boundaries
    use equations     , only : update_magnetic_variables
#endif /* BFIELD */
    use equations     , only : update_primitive_variables
    use equations     , only : fix_unphysical_cells
#ifdef SHOCK_DETECTION
    use equations     , only : detect_shock
#endif /* SHOCK_DETECTION */
#ifdef DEBUG
    use equations     , only : check_positivity
#endif /* DEBUG */
#ifdef SHAPES
    use shapes        , only : update_shapes
#endif /* SHAPES */

! local variables are not implicit by default
!
    implicit none

! arguments
!
    real, intent(in) :: dtt
!
!-------------------------------------------------------------------------------
!
#ifdef BFIELD
! update vector potential boundaries
!
    call update_magnetic_boundaries(dtt)

! update magnetic variables
!
    call update_magnetic_variables()
#endif /* BFIELD */

! update primitive variables
!
    call update_primitive_variables()

! update boundaries
!
    call update_boundaries(dtt)

#ifdef SHAPES
! reset the update for special regions
!
    call update_shapes()
#endif /* SHAPES */

! correct unphysical cells
!
    call fix_unphysical_cells()

#ifdef DEBUG
! check variable positivity
!
    call check_positivity()
#endif /* DEBUG */

#ifdef SHOCK_DETECTION
! find shock locations
!
    call detect_shock()
#endif /* SHOCK_DETECTION */

!-------------------------------------------------------------------------------
!
  end subroutine update_variables
!
!===============================================================================
!
! subroutine EVOLVE_EULER:
! -----------------------
!
!   Subroutine evolves system by one time step using the Euler method.
!
!   References:
!
!     [1] Liu, X.-D. & Osher, S., 1998, J. Comput. Phys., 141, 1
!
!===============================================================================
!
  subroutine evolve_euler()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
! update variables from the conservative scheme
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the first RK substep
!
    u0( : ,:,:,:) = u0( : ,:,:,:) + dt * du( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph,:,:,:) = decay * u0(iph,:,:,:)
#endif /* GLM */

#ifdef FORCING
! add forcing source terms
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_euler
!
!===============================================================================
!
! subroutine EVOLVE_RK2:
! ---------------------
!
!   Subroutine evolves system by one time step using the 2nd order Runge Kutta
!   method.
!
!   References:
!
!     [1] Liu, X.-D. & Osher, S., 1998, J. Comput. Phys., 141, 1
!
!===============================================================================
!
  subroutine evolve_rk2()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt, dtt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, u1, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
!! 1st step of integration
!!
! prepare fractional time step
!
    dtt = dt

! update variables from the conservative scheme
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u1( : ,:,:,:) = u0( : ,:,:,:) + dtt * du( : ,:,:,:)

! prepare pointer to the current array for the next integration step
!
    uu => u1

! update variables
!
    call update_variables(dtt)

!! 2nd step of integration
!!
! prepare fractional time step
!
    dtt = 0.5d+00 * dt

! update variables from the conservative scheme
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the last integration step
!
    u0( : ,:,:,:) = 0.5d+00 * (u0( : ,:,:,:) + u1( : ,:,:,:))                  &
                                                         + dtt * du( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph,:,:,:) = decay * u0(iph,:,:,:)
#endif /* GLM */

! assign the pointer to the first array
!
    uu => u0

#ifdef FORCING
! add forcing source terms
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dtt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_rk2
!
!===============================================================================
!
! subroutine EVOLVE_RK2_N:
! -----------------------
!
!   Subroutine evolves system by one time step using the 2nd order low-storage
!   optimal explicit strong stability preserving Runge Kutta (SSPRK) method.
!
!   References:
!
!     [1] Ketcheson, 2008, SIAM J. Sci. Comput. 30, 4, 2113
!
!===============================================================================
!
  subroutine evolve_rk2_n()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt, dtt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, u1, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none

! flag for the first run, and integration coefficients
!
    logical, save :: first = .true.
    real   , save :: ft, fl, fr
    integer       :: n
!
!-------------------------------------------------------------------------------
!
! initialize the integration parameters
!
    if (first) then

      ft    = 1.0d+00 / (nstages - 1)
      fl    = 1.0d+00 /  nstages
      fr    = 1.0d+00 - fl

      first = .false.

    end if

! = copy the initial state to U(1) =
!
    u1( : ,:,:,:) = u0( : ,:,:,:)

! assign the pointer to the current array
!
    uu => u1

! calculate fractional time step
!
    dtt = ft * dt

! = integrate in the loop (n-1) times =
!
    do n = 1, nstages - 1

! solve the Riemann problem
!
      call update()

! add explicit source terms
!
      call update_sources()

! update variables for the intermediate integration step
!
      u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
      call update_variables(dtt)

    end do

! calculate fractional time step for the last stage
!
    dtt = fl * dt

! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the last integration step
!
    u0( : ,:,:,:) = fl * u0( : ,:,:,:) + fr * u1( : ,:,:,:)                    &
                                                         + dtt * du( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph,:,:,:) = decay * u0(iph,:,:,:)
#endif /* GLM */

! assign the pointer to the first array
!
    uu => u0

#ifdef FORCING
! add forcing source terms
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dtt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_rk2_n
!
!===============================================================================
!
! subroutine EVOLVE_RK3:
! ---------------------
!
!   Subroutine evolves system by one time step using the 3rd order three stage
!   explicit strong stability preserving Runge Kutta method.
!
!   References:
!
!     [1] Liu, X.-D. & Osher, S., 1998, J. Comput. Phys., 141, 1
!     [2] Gotlieb, Ketcheson, & Shu, 2009, J. Sci. Comput., 38, 251
!
!   U(1)   = U(n) + dt * F[U(n)]
!   U(2)   = (3 * U(n) + U(1) + dt * F[U(1)]) / 4
!   U(n+1) = (U(n) + 2 * U(2) + 2 * dt * F[U(2)]) / 3
!
!===============================================================================
!
  subroutine evolve_rk3()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt, dtt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, u1, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none

! local integration parameters
!
    real, parameter :: f21 = 3.0d+00 / 4.0d+00, f22 = 1.0d+00 / 4.0d+00
    real, parameter :: f31 = 1.0d+00 / 3.0d+00, f32 = 2.0d+00 / 3.0d+00
!
!-------------------------------------------------------------------------------
!
!! 1st substep of integration
!!
! prepare fractional time step
!
    dtt = dt

! update variables from the conservative scheme
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u1( : ,:,:,:) = u0( : ,:,:,:) + dtt * du( : ,:,:,:)

! prepare pointer to the current array for the next integration step
!
    uu => u1

! update variables
!
    call update_variables(dtt)

!! 2nd substep of integration
!!
! prepare fractional time step
!
    dtt = f22 * dt

! update variables from the conservative scheme
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u1( : ,:,:,:) = f21 * u0( : ,:,:,:) + f22 * u1( : ,:,:,:)                  &
                                                         + dtt * du( : ,:,:,:)

! update variables
!
    call update_variables(dtt)

!! 3rd substep of integration
!!
! prepare fractional time step
!
    dtt = f32 * dt

! update variables from the conservative scheme
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the last integration step
!
    u0( : ,:,:,:) = f31 * u0( : ,:,:,:) + f32 * u1( : ,:,:,:)                  &
                                                         + dtt * du( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph,:,:,:) = decay * u0(iph,:,:,:)
#endif /* GLM */

! prepare pointer to the original array
!
    uu => u0

#ifdef FORCING
! source terms from turbulence driving
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dtt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_rk3
!
!===============================================================================
!
! subroutine EVOLVE_RK3_4:
! -----------------------
!
!   Subroutine evolves system by one time step using the 3rd order four stage
!   explicit strong stability preserving Runge Kutta method.
!
!   References:
!
!     [1] Ruuth, 2006, Math. Comp., 75, 183
!
!===============================================================================
!
  subroutine evolve_rk3_4()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt, dtt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, u1, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none

! local integration parameters
!
    real, parameter :: fh = 1.0d+00 / 2.0d+00, fo = 1.0d+00 / 3.0d+00
    real, parameter :: ft = 2.0d+00 / 3.0d+00, fs = 1.0d+00 / 6.0d+00
!
!-------------------------------------------------------------------------------
!
! = 1st step of the integration: U(1) = U(n) + 1/2 dt F[U(n)] =
!
! calculate fractional time step
!
    dtt = fh * dt

! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u1( : ,:,:,:) = u0( : ,:,:,:) + dtt * du( : ,:,:,:)

! prepare pointer to the current array for the next integration step
!
    uu => u1

! update variables
!
    call update_variables(dtt)

!
! = 2nd step of the integration: U(2) = U(1) + 1/2 dt F[U(1)] =
! (we can copy U(2) directly to U(1), since U(1) is not used anymore)
!
! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
    call update_variables(dtt)

!
! = 3rd step of the integration: U(3) = 2/3 U(n) + 1/3 U(2) + 1/6 dt F[U(2)] =
! (we can copy U(3) directly to U(1), since U(1) is not used anymore)
!
! calculate fractional time step
!
    dtt = fs * dt

! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the last integration step
!
    u1( : ,:,:,:) = ft * u0( : ,:,:,:) + fo * u1( : ,:,:,:)                    &
                                                         + dtt * du( : ,:,:,:)

! update variables
!
    call update_variables(dtt)

!
! = update the final solution: U(n+1) = U(3) + 1/2 dt F[U(3)]
!
! calculate fractional time step
!
    dtt = fh * dt

! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the last integration step
!
    u0( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph,:,:,:) = decay * u0(iph,:,:,:)
#endif /* GLM */

! prepare pointer to the original array
!
    uu => u0

#ifdef FORCING
! add the source term from turbulence driving ar the last step
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dtt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_rk3_4
!
!===============================================================================
!
! subroutine EVOLVE_RK3_N:
! -----------------------
!
!   Subroutine evolves the system by one time step using the 3rd order
!   low-storage optimal explicit strong stability preserving Runge Kutta
!   (SSPRK) method.  The number Runge-Kutta of stages can be chosen by setting
!   nbase to a value equal or bigger than 2.  The number of stages is nbase².
!   This method has an effective CFL coefficient CFLeff = (1 - 1 / nbase).
!
!   References:
!
!     [1] Ketcheson, 2008, SIAM J. Sci. Comput. 30, 4, 2113
!
!===============================================================================
!
  subroutine evolve_rk3_n()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt, dtt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, u1, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none

! flag for the first run, and integration coefficients
!
    logical, save :: first = .true.
    integer, save :: r, n1l, n1u, n2l, n2u, n3l, n3u
    real   , save :: ft, fl, fr
    integer       :: n
!
!-------------------------------------------------------------------------------
!
! initialize the integration parameters
!
    if (first) then

      r     = (nbase - 1) * nbase
      n1l   = 1
      n1u   = (nbase - 1) * (nbase - 2) / 2
      n2l   = n1u + 1
      n2u   = nbase * (nbase + 1) / 2 - 1
      n3l   = n2u + 2
      n3u   = nbase * nbase

      fl    = (1.0d+00 * nbase) / (2 * nbase - 1)
      fr    = 1.0d+00 - fl
      ft    = fr / r

      first = .false.

    end if

! = copy the initial state to U(1) =
!
    u1( : ,:,:,:) = u0( : ,:,:,:)

! assign the pointer to the current array
!
    uu => u1

! calculate fractional time step
!
    dtt = dt / r

! = integrate in the first loop nl times =
!
    do n = n1l, n1u

! solve the Riemann problem
!
      call update()

! add explicit source terms
!
      call update_sources()

! update variables for the intermediate integration step
!
      u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
      call update_variables(dtt)

    end do

! = copy the evolved U(1) to U(0) =
!
    u0( : ,:,:,:) = u1( : ,:,:,:)

! = integrate in the second loop (nu - nl) times =
!
    do n = n2l, n2u

! solve the Riemann problem
!
      call update()

! add explicit source terms
!
      call update_sources()

! update variables for the intermediate integration step
!
      u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
      call update_variables(dtt)

    end do

! calculate fractional time step for the last stage
!
    dtt = ft * dt

! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u1( : ,:,:,:) = fl * u0( : ,:,:,:) + fr * u1( : ,:,:,:)                    &
                                                         + dtt * du( : ,:,:,:)

! update variables
!
    call update_variables(dtt)

! calculate fractional time step
!
    dtt = dt / r

! = integrate in the third loop (nbase * nbase - nu) times =
!
    do n = n3l, n3u

! solve the Riemann problem
!
      call update()

! add explicit source terms
!
      call update_sources()

! update variables for the intermediate integration step
!
      u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
      call update_variables(dtt)

    end do

! = copy the solution from U(1) to U(0) =
!
    u0( : ,:,:,:) = u1( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph    ,:,:,:) = decay * u0(iph    ,:,:,:)
#endif /* GLM */

! assign the pointer to the first array
!
    uu => u0

#ifdef FORCING
! add forcing source terms
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dtt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_rk3_n
!
!===============================================================================
!
! subroutine EVOLVE_RK4_10:
! ------------------------
!
!   Subroutine evolves system by one time step using the 4th order ten stage
!   explicit strong stability preserving Runge Kutta method.
!
!   References:
!
!     [1] Ketcheson, 2008, SIAM J. Sci. Comp., 40, 4, 2113
!
!===============================================================================
!
  subroutine evolve_rk4_10()

! include external procedures and variables
!
#ifdef FORCING
    use forcing       , only : drive
#endif /* FORCING */
    use mesh          , only : dt, dtt
    use schemes       , only : update
    use sources       , only : update_sources
    use variables     , only : uu, u0, u1, du
#ifdef GLM
    use variables     , only : iph
#endif /* GLM */

! local variables are not implicit by default
!
    implicit none

! local variables
!
    integer :: n

! local integration parameters
!
    real, parameter :: fs = 1.0d+00 /  6.0d+00, fo = 1.0d+00 / 25.0d+00
    real, parameter :: fn = 9.0d+00 / 25.0d+00, ft = 3.0d+00 /  5.0d+00
!
!-------------------------------------------------------------------------------
!
! = copy the initial state to U(1) =
!
    u1( : ,:,:,:) = u0( : ,:,:,:)

! assign the pointer with the current array
!
    uu => u1

! calculate fractional time step
!
    dtt = fs * dt

!
! = first loop: U(1) = U(1) + 1/6 dt F[U(1)] =
!
    do n = 1, 5

! solve the Riemann problem
!
      call update()

! add explicit source terms
!
      call update_sources()

! update variables for the intermediate integration step
!
      u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
      call update_variables(dtt)

    end do

!
! = the second step: U(2) = 1/25 U(2) + 9/25 U(1) =
!
    u0( : ,:,:,:) = fo * u0( : ,:,:,:) + fn * u1( : ,:,:,:)

! = the third step: U(1) = 15 U(2) - 5.0 U(1) =
!
    u1( : ,:,:,:) = 15.0d+00 * u0( : ,:,:,:) - 5.0d+00 * u1( : ,:,:,:)

! assign the pointer with the current array
!
    uu => u1

! update variables
!
    call update_variables(dtt)

!
! = second loop: U(1) = U(1) + 1/6 dt F[U(1)] =
!
    do n = 6, 9

! solve the Riemann problem
!
      call update()

! add explicit source terms
!
      call update_sources()

! update variables for the intermediate integration step
!
      u1( : ,:,:,:) = u1( : ,:,:,:) + dtt * du( : ,:,:,:)

! update variables
!
      call update_variables(dtt)

    end do

!
! = the last step =
!
! calculate fractional time step
!
    dtt = 0.1d+00 * dt

! solve the Riemann problem
!
    call update()

! add explicit source terms
!
    call update_sources()

! update variables for the intermediate integration step
!
    u0( : ,:,:,:) = u0( : ,:,:,:) + ft * u1( : ,:,:,:) + dtt * du( : ,:,:,:)

#ifdef GLM
! update ψ by its source term
!
    u0(iph,:,:,:) = decay * u0(iph,:,:,:)
#endif /* GLM */

! assign the pointer with the initial array
!
    uu => u0

#ifdef FORCING
! add the source term from turbulence driving ar the last step
!
    call drive()
#endif /* FORCING */

! update variables
!
    call update_variables(dtt)

!-------------------------------------------------------------------------------
!
  end subroutine evolve_rk4_10

!===============================================================================
!
end module evolution
